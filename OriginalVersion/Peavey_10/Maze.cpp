#include "Maze.h"


void Maze::up(char map[][42], int &player_xcoordinate, int &player_ycoordinate)
{
	int previous_x = player_xcoordinate;
	int previous_y = player_ycoordinate;
	if (map[player_xcoordinate - 1][player_ycoordinate] == '+')
	{
	}
	else if (map[player_xcoordinate - 1][player_ycoordinate] == '-')
	{
	}
	else if (map[player_xcoordinate - 1][player_ycoordinate] == '|')
	{
	}
	else
	{
		player_xcoordinate--;
		map[player_xcoordinate][player_ycoordinate] = '@';
		map[previous_x][previous_y] = ' ';
	}
}

void Maze::right(char map[][42], int &player_xcoordinate, int &player_ycoordinate)
{
	int previous_x = player_xcoordinate;
	int previous_y = player_ycoordinate;
	if (map[player_xcoordinate][player_ycoordinate + 1] == '|')
	{
	}
	else if (map[player_xcoordinate][player_ycoordinate + 1] == '+')
	{
	}
	else if (map[player_xcoordinate][player_ycoordinate + 1] == '-')
	{
	}
	else
	{
		player_ycoordinate++;
		map[player_xcoordinate][player_ycoordinate] = '@';
		map[previous_x][previous_y] = ' ';
	}
}

void Maze::down(char map[][42], int &player_xcoordinate, int &player_ycoordinate)
{

	int previous_x = player_xcoordinate;
	int previous_y = player_ycoordinate;
	if (map[player_xcoordinate + 1][player_ycoordinate] == '+')
	{
	}
	else if (map[player_xcoordinate + 1][player_ycoordinate] == '-')
	{
	}
	else if (map[player_xcoordinate + 1][player_ycoordinate] == '|')
	{
	}
	else
	{
		player_xcoordinate++;
		map[player_xcoordinate][player_ycoordinate] = '@';
		map[previous_x][previous_y] = ' ';
	}
}

void Maze::left(char map[][42], int &player_xcoordinate, int &player_ycoordinate)
{
	int previous_x = player_xcoordinate;
	int previous_y = player_ycoordinate;
	if (map[player_xcoordinate][player_ycoordinate - 1] == '|')
	{
	}
	else if (map[player_xcoordinate][player_ycoordinate - 1] == '-')
	{
	}
	else if (map[player_xcoordinate][player_ycoordinate - 1] == '+')
	{
	}
	else
	{
		player_ycoordinate--;
		map[player_xcoordinate][player_ycoordinate] = '@';
		map[previous_x][previous_y] = ' ';
	}
}
