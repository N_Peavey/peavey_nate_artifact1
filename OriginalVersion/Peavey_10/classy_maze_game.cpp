#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include "Maze.h"
#include "Map.h"
using namespace std;

//Maze Map
char maze_map[42][42] = { { "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" },{ "|@|     |   |   |   |         |       | |" },
{ "+ +-+-+ +-+ + + + + +-+-+-+ + + +-+-+ + +" },{ "|   |     |   |   |         |   |   | | |" },
{ "+-+ + + + + +-+-+-+-+-+-+-+-+-+-+-+ + + +" },{ "|   | | | | |           |     |       | |" },
{ "+ +-+ +-+ + + +-+-+-+-+ + +-+ + +-+-+ + +" },{ "| |     |   | |     | | | | |   |   |   |" },
{ "+ + +-+ + +-+ +-+ + + + + + +-+-+ +-+-+-+" },{ "|   |   |   |   | |     |             | |" },
{ "+-+-+ +-+-+-+-+ +-+-+ +-+-+ +-+-+ +-+ + +" },{ "|   |   |       |   |     | |   |   | | |" },
{ "+ + +-+ + +-+-+-+ + +-+ + +-+ + +-+ + + +" },{ "| |     |   |     | |   |   | |     | | |" },
{ "+ +-+-+-+-+ + +-+-+ + +-+-+ + +-+-+-+ + +" },{ "|       |   | |   | | |   |   |     |   |" },
{ "+-+ +-+-+ +-+ + + + +-+ + +-+ +-+-+ +-+ +" },{ "|   |   |   |   | | |   |   | |     | | |" },
{ "+ +-+ + +-+ +-+ + + + +-+-+ + + + +-+ + +" },{ "|   | |   |   | | |   |   | | | | | | | |" },
{ "+-+ +-+ + +-+ +-+ + +-+ +-+ +-+ + + + + +" },{ "|   |   |   |   | |   | |       |   |   |" },
{ "+ +-+ +-+-+-+-+ + + + + + + +-+-+-+-+-+-+" },{ "| |   | |     | | | | |   | |       |   |" },
{ "+ + +-+ + +-+ + + + + + +-+ + +-+-+ + +-+" },{ "| | |   |   |     | | | |   |     | |   |" },
{ "+ + + + +-+ +-+-+-+-+ +-+ +-+-+-+-+ +-+ +" },{ "| | | |     |   |   | |   |       | |   |" },
{ "+ + + +-+-+-+ +-+ + + + + + +-+-+ + + +-+" },{ "|   |       |     |   | |       |   |   |" },
{ "+-+ +-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+ +" },{ "|   |   | |     |         |       |   | |" },
{ "+ +-+ + + + +-+ +-+ +-+-+ + +-+-+ +-+ + +" },{ "| | | |     | |     | |   | |   |   |   |" },
{ "+ + + +-+-+-+ +-+-+-+ + +-+ +-+ +-+ + + +" },{ "|   | |   |     |   |   |     | | |   | |" },
{ "+ +-+ + +-+ +-+ +-+ +-+-+-+ + + + +-+-+ +" },{ "| |   |     |   |   |   |   | | |     | |" },
{ "+ + +-+ +-+-+ +-+ + + + + +-+ + + +-+ + +" },{ "| |         |     |   |     |   |   |  X|" },
{ "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" } };

//main loop
void main()
{
	Maze movement;
	Map map;
	//Starting player coords
	int player_xcoordinate = map.Start_YCoord();
	int player_ycoordinate = map.Start_YCoord();
	int move_counter = 0;
	char move;
	//Start loop
	do
	{
		system("cls");
		//Display maze map
		map.displayMap(maze_map);
		//Enter move
		cout << "Enter your move (w = Up, d = Right, s = Down, a = Left): ";
		move = _getch();
		//Make the move
		if (move == 'w' || move == 'W')
		{
			movement.up(maze_map, player_xcoordinate, player_ycoordinate);
			move_counter++;
		}
		else if (move == 'd' || move == 'D')
		{
			movement.right(maze_map, player_xcoordinate, player_ycoordinate);
			move_counter++;
		}
		else if (move == 's' || move == 'S')
		{
			movement.down(maze_map, player_xcoordinate, player_ycoordinate);
			move_counter++;
		}
		else if (move == 'a' || move == 'A')
		{
			movement.left(maze_map, player_xcoordinate, player_ycoordinate);
			move_counter++;
		}
		else
		{
			cout << "Invalid Input";
		}

	} while (maze_map[player_xcoordinate][player_ycoordinate] != maze_map[39][39]);
	system("cls");
	map.displayMap(maze_map);
	cout << "You Win!!" << endl;
	cout << "Total Moves Made: " << move_counter;
	_getch();
}
