#pragma once
class Maze
{
public:
	void up(char map[][42], int &player_xcoordinate, int &player_ycoordinate);
	void right(char map[][42], int &player_xcoordinate, int &player_ycoordinate);
	void down(char map[][42], int &player_xcoordinate, int &player_ycoordinate);
	void left(char map[][42], int &player_xcoordinate, int &player_ycoordinate);
};