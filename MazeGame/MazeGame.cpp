//Nate Peavey
//SNHU

#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include "Maze.h"

using namespace std;

int main()
{
	//maze class
	Maze game;

	//variable to hold player input
	char move;

	//loop until player reaches X in the maze
	do
	{
		system("cls");
		//Display maze map
		game.displayMap();
		//Output move prompt
		cout << "Make it to the X mark!" << endl;
		cout << "Enter your move (w = Up, d = Right, s = Down, a = Left): ";
		//Read input
		move = _getch();
		//Attempt to make move
		game.movement(move);

	} while (game.mazeMap[game.playerYCoordinate][game.playerXCoordinate] != game.mazeMap[game.endYCoordinate][game.endXCoordinate]);
	system("cls");
	game.displayMap();
	cout << "You Win!!" << endl;
	cout << "Total Moves Made: " << game.moveCounter;
	system("pause");
}
