#include "Maze.h"

// initialize class and set up game
Maze::Maze()
{
	moveCounter = 0;

	srand((unsigned int)time(NULL));

	chooseMaze();
	startPosition();
	endPosition();
	playerXCoordinate = startXCoordinate;
	playerYCoordinate = startYCoordinate;
	mazeMap[playerYCoordinate][playerXCoordinate] = '@';
	mazeMap[endYCoordinate][endXCoordinate] = 'X';
	
}

// randomly select one of three mazes
void Maze::chooseMaze()
{
	mazeNumber = rand() % 3 + 1;
	if (mazeNumber == 1)
	{
		memcpy(mazeMap, mazeMapOne, 42 * 42 * sizeof(char));
	}
	else if (mazeNumber == 2)
	{
		memcpy(mazeMap, mazeMapTwo, 42 * 42 * sizeof(char));
	}
	else if (mazeNumber == 3)
	{
		memcpy(mazeMap, mazeMapThree, 42 * 42 * sizeof(char));
	}
}

// randomly find valid start position
void Maze::startPosition()
{
	bool startSuccess = false;
	do
	{
		startXCoordinate = rand() % 39 + 1;
		startYCoordinate = rand() % 39 + 1;

		if (mazeMap[startYCoordinate][startXCoordinate] == '+')
		{
			// invalid move
		}
		else if (mazeMap[startYCoordinate][startXCoordinate] == '-')
		{
			// invalid move
		}
		else if (mazeMap[startYCoordinate][startXCoordinate] == '|')
		{
			// invalid move
		}
		else
		{
			startSuccess = true;
		}
	} while (startSuccess == false);
}

// randomly find valid end position
void Maze::endPosition()
{
	bool endSuccess = false;
	do
	{
		endXCoordinate = rand() % 39 + 1;
		endYCoordinate = rand() % 39 + 1;

		if (mazeMap[endYCoordinate][endXCoordinate] == '+')
		{
			// invalid move
		}
		else if (mazeMap[endYCoordinate][endXCoordinate] == '-')
		{
			// invalid move
		}
		else if (mazeMap[endYCoordinate][endXCoordinate] == '|')
		{
			// invalid move
		}
		else
		{
			endSuccess = true;
		}
	} while (endSuccess == false);
}

// output to display maze map
// x and y may seem backwards but with the way arrays work they are actually correct
void Maze::displayMap()
{
	for (int y = 0; y < 42; y++)
	{
		for (int x = 0; x < 42; x++)
		{
			cout << mazeMap[y][x] << " ";
		}
		cout << endl;
	}
}

// read input and attempt movement
void Maze::movement(char moveChoice)
{
	int previousX = playerXCoordinate;
	int previousY = playerYCoordinate;

	// move up
	if (moveChoice == 'w' || moveChoice == 'W')
	{
		if (mazeMap[playerYCoordinate - 1][playerXCoordinate] == '+')
		{
			// invalid move
		}
		else if (mazeMap[playerYCoordinate - 1][playerXCoordinate] == '-')
		{
			// invalid move
		}
		else if (mazeMap[playerYCoordinate - 1][playerXCoordinate] == '|')
		{
			// invalid move
		}
		else
		{
			playerYCoordinate--;
			mazeMap[playerYCoordinate][playerXCoordinate] = '@';
			mazeMap[previousY][previousX] = ' ';
		}
		moveCounter++;
	}

	// move right
	else if (moveChoice == 'd' || moveChoice == 'D')
	{
		if (mazeMap[playerYCoordinate][playerXCoordinate + 1] == '+')
		{
			// invalid move
		}
		else if (mazeMap[playerYCoordinate][playerXCoordinate + 1] == '-')
		{
			// invalid move
		}
		else if (mazeMap[playerYCoordinate][playerXCoordinate + 1] == '|')
		{
			// invalid move
		}
		else
		{
			playerXCoordinate++;
			mazeMap[playerYCoordinate][playerXCoordinate] = '@';
			mazeMap[previousY][previousX] = ' ';
		}
		moveCounter++;
	}

	// move down
	else if (moveChoice == 's' || moveChoice == 'S')
	{
		if (mazeMap[playerYCoordinate + 1][playerXCoordinate] == '+')
		{
			// invalid move
		}
		else if (mazeMap[playerYCoordinate + 1][playerXCoordinate] == '-')
		{
			// invalid move
		}
		else if (mazeMap[playerYCoordinate + 1][playerXCoordinate] == '|')
		{
			// invalid move
		}
		else
		{
			playerYCoordinate++;
			mazeMap[playerYCoordinate][playerXCoordinate] = '@';
			mazeMap[previousY][previousX] = ' ';
		}
		moveCounter++;
	}

	// move left
	else if (moveChoice == 'a' || moveChoice == 'A')
	{
		if (mazeMap[playerYCoordinate][playerXCoordinate - 1] == '+')
		{
			// invalid move
		}
		else if (mazeMap[playerYCoordinate][playerXCoordinate - 1] == '-')
		{
			// invalid move
		}
		else if (mazeMap[playerYCoordinate][playerXCoordinate - 1] == '|')
		{
			// invalid move
		}
		else
		{
			playerXCoordinate--;
			mazeMap[playerYCoordinate][playerXCoordinate] = '@';
			mazeMap[previousY][previousX] = ' ';
		}
		moveCounter++;
	}
	else
	{
		cout << "Invalid Input";
	}
}