#pragma once
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
using namespace std;

class Maze
{
public:
	// initialize class
	Maze();

	// class variable
	int moveCounter;
	int mazeNumber;
	int playerXCoordinate;
	int playerYCoordinate;
	int startXCoordinate;
	int startYCoordinate;
	int endXCoordinate;
	int endYCoordinate;
	char mazeMap[42][42];

	// Maze One
	char mazeMapOne[42][42] = 
	{
		{ "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" }, { "| |     |   |   |   |         |       | |" },
		{ "+ +-+-+ +-+ + + + + +-+-+-+ + + +-+-+ + +" }, { "|   |     |   |   |         |   |   | | |" },
		{ "+-+ + + + + +-+-+-+-+-+-+-+-+-+-+-+ + + +" }, { "|   | | | | |           |     |       | |" },
		{ "+ +-+ +-+ + + +-+-+-+-+ + +-+ + +-+-+ + +" }, { "| |     |   | |     | | | | |   |   |   |" },
		{ "+ + +-+ + +-+ +-+ + + + + + +-+-+ +-+-+-+" }, { "|   |   |   |   | |     |             | |" },
		{ "+-+-+ +-+-+-+-+ +-+-+ +-+-+ +-+-+ +-+ + +" }, { "|   |   |       |   |     | |   |   | | |" },
		{ "+ + +-+ + +-+-+-+ + +-+ + +-+ + +-+ + + +" }, { "| |     |   |     | |   |   | |     | | |" },
		{ "+ +-+-+-+-+ + +-+-+ + +-+-+ + +-+-+-+ + +" }, { "|       |   | |   | | |   |   |     |   |" },
		{ "+-+ +-+-+ +-+ + + + +-+ + +-+ +-+-+ +-+ +" }, { "|   |   |   |   | | |   |   | |     | | |" },
		{ "+ +-+ + +-+ +-+ + + + +-+-+ + + + +-+ + +" }, { "|   | |   |   | | |   |   | | | | | | | |" },
		{ "+-+ +-+ + +-+ +-+ + +-+ +-+ +-+ + + + + +" }, { "|   |   |   |   | |   | |       |   |   |" },
		{ "+ +-+ +-+-+-+-+ + + + + + + +-+-+-+-+-+-+" }, { "| |   | |     | | | | |   | |       |   |" },
		{ "+ + +-+ + +-+ + + + + + +-+ + +-+-+ + +-+" }, { "| | |   |   |     | | | |   |     | |   |" },
		{ "+ + + + +-+ +-+-+-+-+ +-+ +-+-+-+-+ +-+ +" }, { "| | | |     |   |   | |   |       | |   |" },
		{ "+ + + +-+-+-+ +-+ + + + + + +-+-+ + + +-+" }, { "|   |       |     |   | |       |   |   |" },
		{ "+-+ +-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+ +" }, { "|   |   | |     |         |       |   | |" },
		{ "+ +-+ + + + +-+ +-+ +-+-+ + +-+-+ +-+ + +" }, { "| | | |     | |     | |   | |   |   |   |" },
		{ "+ + + +-+-+-+ +-+-+-+ + +-+ +-+ +-+ + + +" }, { "|   | |   |     |   |   |     | | |   | |" },
		{ "+ +-+ + +-+ +-+ +-+ +-+-+-+ + + + +-+-+ +" }, { "| |   |     |   |   |   |   | | |     | |" },
		{ "+ + +-+ +-+-+ +-+ + + + + +-+ + + +-+ + +" }, { "| |         |     |   |     |   |   |   |" },
		{ "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" }
	};

	// Maze Two
	char mazeMapTwo[42][42] =
	{
		{ "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" }, { "|     |   |     |   |   |           |   |" },
		{ "+ +-+ +-+ + +-+ + + + +-+ + +-+ +-+ +-+ +" }, { "|   |   | |   | | |       |   | | |   | |" },
		{ "+-+-+-+ + +-+ + + +-+-+ +-+-+ + + + + + +" }, { "|       | |   |   |   |   |   |   | |   |" },
		{ "+ +-+-+-+ + +-+-+-+ + +-+ + +-+-+ + +-+-+" }, { "| |         | |   | |   | | |   | |     |" },
		{ "+ + +-+-+-+-+ + + + +-+ +-+ + + + +-+-+ +" }, { "| |   | |     | | |   |   | | |   |   | |" },
		{ "+ +-+ + + +-+ + + +-+ +-+ + +-+-+ + + + +" }, { "| |     | | | | |   |   | |     |   | | |" },
		{ "+ +-+-+-+ + + + +-+ + + + +-+-+ +-+-+ + +" }, { "|         | |   |   | | |     | | |   | |" },
		{ "+ +-+-+-+-+ +-+ + +-+-+ +-+-+ + + + +-+-+" }, { "|   | |     | | | |     |   | |   |     |" },
		{ "+-+ + + + +-+ + + +-+-+-+-+ + + +-+-+ + +" }, { "| | | | | |     |     |     |   |   | | |" },
		{ "+ + + + + +-+-+-+-+-+ + +-+ +-+-+ + +-+ +" }, { "|   | | |           | | |   |     |     |" },
		{ "+-+ + + +-+-+-+-+ + + + +-+-+ +-+ +-+-+ +" }, { "|   | | |   |     | | |     | |   |     |" },
		{ "+ +-+ + + + + +-+-+-+ + +-+ + + +-+-+-+-+" }, { "|   |   | | | | |     |   | | | |       |" },
		{ "+-+ + +-+ + + + + +-+-+-+-+ + + + + + +-+" }, { "| |   |   |   |   |     |   | |   | |   |" },
		{ "+ + +-+ +-+-+-+ +-+-+-+ + +-+ +-+-+-+-+ +" }, { "|   |   |   |   |         | |   |       |" },
		{ "+-+-+ +-+ + + +-+ +-+-+ +-+ +-+ +-+-+-+ +" }, { "|       | | |     |   | |   |   |     | |" },
		{ "+ +-+ + +-+ + +-+-+ + + + + + +-+ +-+ + +" }, { "| |   |   | |   |   | | | |   |   | |   |" },
		{ "+ + +-+-+ + +-+ + +-+ + + +-+-+ +-+ + +-+" }, { "| | |   |   | | |   | | | |   | |   |   |" },
		{ "+ + + + +-+-+ + +-+-+ + +-+ + + + + +-+ +" }, { "| | | |       |   |   |   | | | | |     |" },
		{ "+ + +-+-+ + +-+-+ + + +-+ + + + + +-+-+-+" }, { "| |       |   | |   |   |   | | | | |   |" },
		{ "+ +-+-+-+-+-+ + +-+ +-+ +-+-+ + + + + + +" }, { "|             |     |       |   |     | |" },
		{ "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" }
	};
	
	// Maze Three
	char mazeMapThree[42][42] =
	{
		{ "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" }, { "|     |         |                   |   |" },
		{ "+ +-+ + + +-+-+-+ +-+-+-+-+-+-+-+-+ + + +" }, { "|   |   | |     | |       |     |   | | |" },
		{ "+ + +-+-+ + +-+ +-+ +-+ + + + + + + + + +" }, { "| |     |   |     |   | |   | | | |   | |" },
		{ "+-+-+-+ +-+-+-+-+ +-+ + +-+-+ + +-+-+ +-+" }, { "|   |   |     |       |     | |     |   |" },
		{ "+ +-+ +-+ +-+ +-+-+-+-+-+-+ + + +-+ +-+ +" }, { "| |   | |   |     |     |   | | | | |   |" },
		{ "+ + +-+ +-+ +-+-+ + +-+ + +-+ + + + + + +" }, { "| |     | | |   |   |     |   |   | | | |" },
		{ "+ +-+-+-+ + + + +-+ +-+-+-+-+-+-+-+ +-+ +" }, { "|   |   | |   |   |       |   |   |     |" },
		{ "+ + +-+ + + +-+-+-+-+-+-+ + + + + + +-+ +" }, { "| | |     |   |   |   | |   |   | | |   |" },
		{ "+ + + +-+ + +-+ + + + + + +-+-+-+ + + + +" }, { "| |     |   |   | | |   |       | | | | |" },
		{ "+ +-+-+-+-+-+ +-+ + +-+ +-+-+-+ + +-+ + +" }, { "| |     |   |   | | | |     |   | |   | |" },
		{ "+ + +-+ +-+ +-+ + + + +-+-+ + + +-+ +-+ +" }, { "| |   |   |     | |   |   | | | |   |   |" },
		{ "+ +-+ +-+ +-+-+-+ +-+-+ + + + +-+ +-+-+ +" }, { "|   |   | |     |   |   | | | |   |     |" },
		{ "+-+ +-+ + + +-+ +-+ +-+-+ + + + +-+ + +-+" }, { "|   |   |   | |           |   | |   |   |" },
		{ "+ + + +-+-+-+ +-+-+-+-+-+ +-+ + + +-+-+ +" }, { "| | | |   |   |   |         |   |   |   |" },
		{ "+-+ + + + + + +-+ +-+-+-+-+ +-+-+-+ + +-+" }, { "|   | | |   |     |         |   |   | | |" },
		{ "+ +-+ + +-+-+-+-+-+ +-+-+-+-+ + + +-+ + +" }, { "|     |   |   |     |     |   |   | |   |" },
		{ "+ +-+-+-+ + + + +-+-+ +-+-+ +-+-+-+ +-+-+" }, { "| |   | | | | |   |   |   |       |   | |" },
		{ "+ +-+ + + + +-+-+ + +-+ + +-+-+-+ +-+ + +" }, { "| |     | |       |     |       |     | |" },
		{ "+ + +-+-+ + +-+ +-+-+-+-+ +-+-+-+-+-+ + +" }, { "| | |     |   | |   |   |         |   | |" },
		{ "+ + + +-+ +-+ + + +-+ + +-+-+-+-+ + +-+ +" }, { "|   |   |     | |     |                 |" },
		{ "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+" }
	};
	

	// functions

	// randomly select one of three mazes
	void chooseMaze();

	// randomly find valid start position
	void startPosition();

	// randomly find valid end position
	void endPosition();

	// output to display maze map
	void displayMap();

	// read input and attempt movement
	void movement(char moveChoice);
};

